import time

from selenium.webdriver import Keys

from pages.base_page import BasePage
from locators.radio_button_locator import RadioButtonLocators as Radio


class RadioButtonPage(BasePage):

    def click_button_yes(self):
        self.element_is_visibility(Radio.YES).click()
        result_text = self.element_is_visibility(Radio.SELECTED)
        time.sleep(5)
        return result_text

    def click_button_impressive(self):
        self.element_is_visibility(Radio.IMPRESSIVE).click()
        result_text = self.element_is_visibility(Radio.SELECTED)
        time.sleep(5)
        return result_text

    def result_text_click(self):
        lst_result_click = self.element_are_visible(Radio.SELECTED)
        return ''.join([i.text for i in lst_result_click])
