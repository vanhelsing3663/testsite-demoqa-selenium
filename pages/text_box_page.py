import time

from selenium.webdriver import Keys

from pages.base_page import BasePage
from locators.text_box_page_locators import TextBoxPageLocators as Txt


class TextBoxPage(BasePage):

    def filling_in_the_fields(self):
        full_name = 'Konnov Kirill Maximovich'
        email = 'konnov360@gmail.com'
        current_address = 'city Yaroslavl Chakolova 1'
        permanent_address = 'none'
        self.remove_footer()
        self.element_is_visibility(Txt.FULL_NAME).send_keys(full_name)
        self.element_is_visibility(Txt.EMAIL).send_keys(email)
        self.element_is_visibility(Txt.CURRENT_ADDRESS).send_keys(current_address)
        self.element_is_visibility(Txt.PERMANENT_ADDRESS).send_keys(permanent_address)
        self.element_is_visibility(Txt.SUBMIT).click()
        time.sleep(2)
        return full_name, email, current_address, permanent_address

    def result_form(self):
        result = self.element_are_visible(Txt.RESULT_FORM)
        return [i.text.split(':')[1] for i in result]
