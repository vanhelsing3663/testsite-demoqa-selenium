import time

from selenium.webdriver import Keys

from pages.base_page import BasePage
from locators.web_table_locators import WebTablePage as Web


class WebTablePage(BasePage):

    def adding_elements_to_the_tablet(self):
        first_name = 'Kirill'
        last_name = 'Konnov'
        email = 'konnov360@gmail.com'
        age = '21'
        salary = '13000'
        departament = 'Yaroslavl'
        search = 'konnov360@gmail.com'
        self.element_is_visibility(Web.ADD).click()
        self.element_is_visibility(Web.FIRST_NAME).send_keys(first_name)
        self.element_is_visibility(Web.LAST_NAME).send_keys(last_name)
        self.element_is_visibility(Web.EMAIL).send_keys(email)
        self.element_is_visibility(Web.AGE).send_keys(age)
        self.element_is_visibility(Web.SALARY).send_keys(salary)
        self.element_is_visibility(Web.DEPARTAMENT).send_keys(departament)
        self.element_is_visibility(Web.SUBMIT).click()
        self.element_is_visibility(Web.SEARCH).send_keys(search)
        time.sleep(5)
        return first_name, last_name, email, age, salary, departament

    def result_web_table_string(self):
        elements = self.element_are_visible(Web.RESULT_STRING)
        return [i.text for i in elements]
