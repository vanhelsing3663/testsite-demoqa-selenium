import time

from selenium.webdriver import Keys

from pages.base_page import BasePage
from locators.form_page_locators import FormPageLocators as Locators


class FormPage(BasePage):

    def fill_fields_and_submit(self):
        first_name = 'Kirill'
        last_name = 'Konnov'
        email = 'konnov360@gmail.com'
        mobile = '1111111111'
        language = 'English'
        path_file = r'C:\Users\Кирилл\PycharmProjects\Selenium_test_site\test_txt.txt'
        city, num, country = 'City', '1234', 'Russia'
        state_n = 'NCR'
        city1 = 'Noida'
        self.remove_footer()
        self.element_is_visibility(Locators.FIRST_NAME).send_keys(first_name)
        self.element_is_visibility(Locators.LAST_NAME).send_keys(last_name)
        self.element_is_visibility(Locators.EMAIL).send_keys(email)
        self.element_is_visibility(Locators.GENDER).click()
        self.element_is_visibility(Locators.MOBILE).send_keys(mobile)
        subject = self.element_is_visibility(Locators.SUBJECT)
        subject.send_keys(language)
        subject.send_keys(Keys.RETURN)
        self.element_is_visibility(Locators.FILE_INPUT).send_keys(path_file)
        self.element_is_visibility(Locators.CURRENT_ADDRESS).send_keys(city, num, country)
        state = self.element_is_visibility(Locators.STATE)
        state.send_keys(state_n)
        state.send_keys(Keys.RETURN)
        city = self.element_is_visibility(Locators.CITY)
        city.send_keys(city1)
        city.send_keys(Keys.RETURN)
        self.element_is_visibility(Locators.SUBMIT).click()
        time.sleep(5)
        return first_name, last_name, email, mobile, language, path_file, city, num, country, state_n, city1

    def form_result(self):
        result_list = self.element_are_visible(Locators.RESULT_TABLE)
        result_text = [i.text for i in result_list]
        return result_text
