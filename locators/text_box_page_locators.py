from selenium.webdriver.common.by import By
from random import randint


class TextBoxPageLocators:
    FULL_NAME = (By.CSS_SELECTOR, '#userName')
    EMAIL = (By.CSS_SELECTOR, '#userEmail')
    CURRENT_ADDRESS = (By.CSS_SELECTOR, '#currentAddress')
    PERMANENT_ADDRESS = (By.CSS_SELECTOR, '#permanentAddress')
    SUBMIT = (By.CSS_SELECTOR, '#submit')
    RESULT_FORM = (By.XPATH, "//div[@class='border col-md-12 col-sm-12']//p[@class='mb-1']")
