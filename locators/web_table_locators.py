from selenium.webdriver.common.by import By
from random import randint


class WebTablePage:
    ADD = (By.CSS_SELECTOR, '#addNewRecordButton')
    FIRST_NAME = (By.CSS_SELECTOR, '#firstName')
    LAST_NAME = (By.CSS_SELECTOR, '#lastName')
    EMAIL = (By.CSS_SELECTOR, '#userEmail')
    AGE = (By.CSS_SELECTOR, '#age')
    SALARY = (By.CSS_SELECTOR, '#salary')
    DEPARTAMENT = (By.CSS_SELECTOR, '#department')
    SUBMIT = (By.CSS_SELECTOR, '#submit')
    SEARCH = (By.CSS_SELECTOR, '#searchBox')
    RESULT_STRING = (By.XPATH, "//div[@class='rt-tr -odd']//div[@class='rt-td']")

