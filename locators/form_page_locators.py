from selenium.webdriver.common.by import By
from random import randint


class FormPageLocators:
    FIRST_NAME = (By.CSS_SELECTOR, '#lastName')
    LAST_NAME = (By.CSS_SELECTOR, '#firstName')
    EMAIL = (By.CSS_SELECTOR, '#userEmail')
    GENDER = (By.CSS_SELECTOR, f"label[for='gender-radio-1']")
    MOBILE = (By.CSS_SELECTOR, '#userNumber')
    SUBJECT = (By.CSS_SELECTOR, '#subjectsInput')
    FILE_INPUT = (By.CSS_SELECTOR, '#uploadPicture')
    CURRENT_ADDRESS = (By.CSS_SELECTOR, '#currentAddress')
    SUBMIT = (By.CSS_SELECTOR, '#submit')
    STATE = (By.CSS_SELECTOR, '#react-select-3-input')
    CITY = (By.CSS_SELECTOR, '#react-select-4-input')
    RESULT_TABLE = (
        By.XPATH, "//table[@class='table table-dark table-striped table-bordered table-hover']/tbody/tr/td[2]")
