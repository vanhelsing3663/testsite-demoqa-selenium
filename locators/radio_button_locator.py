from selenium.webdriver.common.by import By
from random import randint


class RadioButtonLocators:
    YES = (By.CSS_SELECTOR, 'label[for="yesRadio"]')
    IMPRESSIVE = (By.CSS_SELECTOR, 'label[for="impressiveRadio"]')
    SELECTED = (By.CSS_SELECTOR, 'p span.text-success')
