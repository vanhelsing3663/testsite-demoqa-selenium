from pages.text_box_page import TextBoxPage


class TestTextBoxPage:

    def test_text_box(self, driver):
        text_page = TextBoxPage(driver, 'https://demoqa.com/text-box')
        text_page.open()
        full_name, email, current_address, permanent_address = text_page.filling_in_the_fields()
        result = text_page.result_form()
        assert full_name == f'{" ".join([i for i in result[0].split(" ")])}'
        assert email == result[1]
        assert current_address == f'{" ".join([i for i in result[2].split(" ")])}'
        assert permanent_address == result[-1]
