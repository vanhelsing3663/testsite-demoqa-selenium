from pages.form_page import FormPage


class TestFormPage:

    def test_form(self, driver):
        form_page = FormPage(driver, 'https://demoqa.com/automation-practice-form')
        form_page.open()
        first_name, last_name, email, mobile, language, path_file, city2, num, country, state_n, city1 = \
            form_page.fill_fields_and_submit()
        result = form_page.form_result()
        assert f'{last_name} {first_name}' == result[0]
        assert email == result[1]
        assert mobile == result[3]
        assert language == result[5]
        assert f'{state_n} {city1}' == result[-1]
        #