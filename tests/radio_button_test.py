from pages.radio_button_page import RadioButtonPage


class TestRadioButtonPage:

    def test_click_yes(self, driver):
        button = RadioButtonPage(driver, 'https://demoqa.com/radio-button')
        button.open()
        button.click_button_yes()
        res = button.result_text_click()
        assert 'Yes' == res

    def test_click_impressive(self, driver):
        button = RadioButtonPage(driver, 'https://demoqa.com/radio-button')
        button.open()
        button.click_button_impressive()
        res = button.result_text_click()
        assert 'Impressive' == res
