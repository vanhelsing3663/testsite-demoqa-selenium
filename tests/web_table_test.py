from pages.web_table_page import WebTablePage


class TestWebTable:

    def test_web_table(self, driver):
        web_page = WebTablePage(driver, 'https://demoqa.com/webtables')
        web_page.open()
        first_name, last_name, email, age, salary, departament = web_page.adding_elements_to_the_tablet()
        result = web_page.result_web_table_string()
        assert first_name == result[0]
        assert last_name == result[1]
        assert email == result[3]
        assert age == result[2]
        assert salary == result[4]
        assert departament == result[5]
